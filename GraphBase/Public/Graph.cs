﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dan.GraphBase
{
    /// <summary>
    /// Represents graph data structure
    /// </summary>
    public class Graph
    {
        internal GraphContainer Container { get; private set; }

        public Graph()
        {
            this.Container = new GraphContainer();
        }

        public void AddVertex(string key)
        {
            AddVertex(key, null);
        }
        public void AddVertex(string key, object tag)
        {
            this.Container.Vertices.Add(new InternalVertex(new Vertex(this, key, tag)));
            OnGraphChanged();
        }

        public void AddEdge(string source, string destination)
        {
            AddEdge(source, destination, 0);
        }

        public void AddEdge(string source, string destination, int weight)
        {
            AddEdge(source, destination, weight, null);
        }
        public void AddEdge(string source, string destination, int weight, object tag)
        {
            var internalEdge = new InternalEdge(this.Container.FindVertexByPublicKey(source),
                this.Container.FindVertexByPublicKey(destination),
                weight);

            internalEdge.PublicObject = new Edge(internalEdge, tag);
            this.Container.FindVertexByPublicKey(source).Edges.Add(internalEdge);
            OnGraphChanged();
        }

        public int VertexCount
        {
            get { return this.Container.Vertices.Count; }
        }

        public IEnumerable<Vertex> Vertices
        {
            get { return Container.Vertices.Select(vertex => vertex.PublicObject); }
        }
        public event EventHandler GraphChanged;

        private void OnGraphChanged()
        {
            var method = GraphChanged;
            if (method != null) method(this, EventArgs.Empty);
        }

        public void Clear()
        {
            this.Container.Vertices.Clear();
            OnGraphChanged();
        }
    }
}