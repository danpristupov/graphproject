﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Dan.GraphBase.Annotations;

namespace Dan.GraphBase
{
    [DebuggerDisplay("Key: {Key}")]
    public class Vertex : INotifyPropertyChanged
    {
        internal Vertex(Graph graph, string key, object tag)
        {
            this.Graph = graph;
            this.Key = key;
            this.Tag = tag;
        }

        internal Vertex(Graph graph, string key)
            : this(graph, key, null)
        {
        }

        private string _key;
        public string Key
        {
            get { return _key; }
            set
            {
                if (_key == value) return;
                _key = value;
                OnPropertyChanged();
            }
        }

        private string _additionalInfo;
        public string AdditionalInfo
        {
            get { return _additionalInfo; }
            set
            {
                if (_additionalInfo == value) return;
                _additionalInfo = value;
                OnPropertyChanged();
            }
        }

        private AlgorythmState _algorythmState;
        public AlgorythmState AlgorythmState
        {
            get { return _algorythmState; }
            set
            {
                if (_algorythmState == value) return;
                _algorythmState = value;
                OnPropertyChanged();
            }
        }

        public object Tag;

        internal Graph Graph { get; private set; }

        internal Func<string> ToStringFunc { get; set; }
        public override string ToString()
        {
            return ToStringFunc != null ? ToStringFunc() : string.Empty;
        }

        public IEnumerable<Edge> Edges
        {
            get
            {
                foreach (var internalVertex in this.Graph.Container.Vertices)
                {
                    if (internalVertex.PublicObject != this) continue;
                    foreach (var internalEdge in internalVertex.Edges)
                    {
                        yield return internalEdge.PublicObject;
                    }
                }
            }
        }

        // Implementation of the INotifyPropertyChanged interface
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum AlgorythmState
    {
        None,
        Start,
        End,
        Result,
        Active,
        Highlighted
    }
}