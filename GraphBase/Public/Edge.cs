﻿using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Dan.GraphBase.Annotations;

namespace Dan.GraphBase
{
    [DebuggerDisplay("Src: {Source} Dst: {Destination}")]
    public class Edge: INotifyPropertyChanged
    {
        internal Edge(InternalEdge internalEdge, object tag)
        {
            this.InternalEdge = internalEdge;
            this.Tag = tag;
        }

        internal InternalEdge InternalEdge { get; private set; }

        public int Weight
        {
            get { return InternalEdge.Weight; }
            set
            {
                if (this.InternalEdge.Weight == value) return;
                this.InternalEdge.Weight = value;
                OnPropertyChanged();
            }
        }

        private AlgorythmState _algorythmState;
        public AlgorythmState AlgorythmState
        {
            get { return _algorythmState; }
            set
            {
                if (_algorythmState == value) return;
                _algorythmState = value;
                OnPropertyChanged();
            }
        }

        public object Tag { get; private set; }

        public Vertex Source
        {
            get { return InternalEdge.Src.PublicObject; }
        }

        public Vertex Destination
        {
            get { return InternalEdge.Dst.PublicObject; }
        }

        // Implementation of the INotifyPropertyChanged interface
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}