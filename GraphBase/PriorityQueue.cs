﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dan.GraphBase
{
    public class PriorityQueue<T>
    {
        int _totalSize;
        readonly SortedDictionary<int, Queue<T>> _storage;

        public PriorityQueue ()
        {
            this._storage = new SortedDictionary<int, Queue<T>> ();
            this._totalSize = 0;
        }

        public int Count { get { return _totalSize; }}

        public bool IsEmpty () // remove?
        {
            return _totalSize == 0;
        }

        public T Dequeue ()
        {
            if (IsEmpty()) throw new InvalidOperationException("The PriorityQueue<T> is empty");

            _totalSize--;
            return _storage.Values.First(_ => _.Count > 0).Dequeue();
//            foreach (var queue in _storage.Values)
//            {
//                // we use a sorted dictionary
//                if (queue.Count > 0)
//                {
//                    _totalSize--;
//                    return queue.Dequeue();
//                }
//            }
//            throw new InvalidOperationException("not supposed to reach here");
        }

        // same as above, except for peek.

        public object Peek ()
        {
            if (IsEmpty ()) throw new InvalidOperationException("The PriorityQueue<T> is empty");

            return _storage.Values.First(_ => _.Count > 0).Peek();

//            foreach (Queue<T> q in _storage.Values) {
//                if (q.Count > 0)
//                    return q.Peek ();
//            }
//            throw new InvalidOperationException("not supposed to reach here");
        }

        public object Dequeue (int prio)
        {
            _totalSize--;
            return _storage[prio].Dequeue ();
        }

        public void Enqueue (T item, int priority)
        {
            if (!_storage.ContainsKey(priority)) _storage.Add(priority, new Queue<T> ());

            _storage[priority].Enqueue (item);
            _totalSize++;
        }
    }
}