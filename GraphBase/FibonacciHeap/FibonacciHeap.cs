﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Dan.GraphBase.FibonacciHeap
{
    using TKey = System.Int32;
    using TValue = System.String;

    [DebuggerDisplay("{_heap.RootListString}")]
    internal class FibonacciHeap
    {
        private Heap _heap;
        public FibonacciHeap()
        {
            _heap = new Heap();
        }

        public Item Insert(TKey key, TValue value)
        {
            // rename the method. Insert usually doesn't return new object
            var newItem = new Item(key, value);
            Insert(_heap, newItem.Node);
            return newItem;
        }

        public int Count
        {
            get { return _heap.Count; }
        }


        [DebuggerStepThrough]
        public Item ExtractMin()
        {
            return ExtractMin(_heap);
        }

        [DebuggerStepThrough]
        public void DecreaseKey(Item node, TKey key)
        {
            DecreaseKey(node.Node, key);
        }

        private void DecreaseKey(Node node, TKey key)
        {
            if (node.Key.CompareTo(key) < 0) throw new InvalidOperationException("New key is greater than the current key");

            node.PublicObject.Key = key;
            var parent = node.Parent;

            if (parent != null && node.Key < parent.Key)
            {
                Cut(_heap, node, parent);
                CascadingCut(_heap, parent);
            }

            if (node.Key.CompareTo(_heap.Min.Key) < 0)
                _heap.Min = node;
        }

        private void Cut(Heap heap, Node node, Node parent)
        {
            // remove node from the child list of parent
            parent.RemoveChild(node);

            // add x to the root list of heap
            heap.Min.AddSibling(node);
            node.Mark = false;
        }

        private void CascadingCut(Heap heap, Node node)
        {
            var parent = node.Parent;
            if (parent == null) return;
            if (node.Mark == false)
                node.Mark = true;
            else
            {
                Cut(heap, node, parent);
                CascadingCut(heap, parent);
            }
        }

        private static Item ExtractMin(Heap heap)
        {
            if (heap.Count == 0) throw new InvalidOperationException("The heap is empty");
            var z = heap.Min;
            foreach (var child in z.Children.ToArray())
            {
                heap.Min.AddSibling(child);
                child.Parent = null;
            }

            // remove z from the root list of heap
            if (z == z.Right) // the only node in the list
            {
                heap.Min = null;
            }
            else
            {
                heap.Min = z.Right;
                z.RemoveItselfFromTheList();
                Consolidate(heap);
            }

            heap.Count--;

            return z.PublicObject;
        }

        private static void Consolidate(Heap heap)
        {
            var degrees = new Node[heap.Count]; // null-terminated array
            foreach (var node in heap.RootList.ToArray())
            {
                var x = node;
                var degree = x.Degree;

                while (degrees[degree] != null)
                {
                    var y = degrees[degree]; // another node with the same degree as x
                    if (x.Key.CompareTo(y.Key) > 0)
                        Swap(ref x, ref y);

                    HeapLink(heap, y, x);
                    degrees[degree] = null;
                    degree++;
                }
                degrees[degree] = x;
            }


            UpdateMinNode(heap, degrees);
        }

        private static void UpdateMinNode(Heap heap, Node[] degrees)
        {
            heap.Min = degrees
                .Where(_ => _ != null)
                .Aggregate((i1, i2) => i1.Key.CompareTo(i2.Key) < 0 ? i1 : i2);
            return;
            heap.Min = null;
            var maxDegree = degrees
                .Where(_ => _ != null)
                .Max(_ => _.Degree);

            for (var i = 0; i <= maxDegree; i++)
            {
                if (degrees[i] == null) continue;
                if (heap.Min == null)
                {
                    // create a root list for H containing just degrees[i]
                    heap.Min = degrees[i];
                }
                else
                {
                    // insert degrees[i] into H's root list
                    if (degrees[i].Key.CompareTo(heap.Min.Key) < 0)
                        heap.Min = degrees[i];
                }
            }
        }

        private static void Swap(ref Node x, ref Node y)
        {
            var tmp = x; // exchange x with y
            x = y;
            y = tmp;
        }

        private static void HeapLink(Heap heap, Node y, Node x)
        {
            y.RemoveItselfFromTheList();

            x.AddChild(y);
            y.Mark = false;
        }

        private static void Insert(Heap heap, Node newNode)
        {
            if (heap.Min == null)
            {
                // create a root list for the heap containing just new node
                newNode.Left = newNode.Right = newNode; // should be removed
                heap.Min = newNode;
            }
            else
            {
                // add newNode into the root list
                heap.Min.AddSibling(newNode);
                if (newNode.Key.CompareTo(heap.Min.Key) < 0)
                    heap.Min = newNode;
            }
            heap.Count++;
        }
    }
}