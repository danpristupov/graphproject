﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Dan.GraphBase.FibonacciHeap
{
    [DebuggerDisplay("{RootListString}")]
    internal class Heap
    {
        public Node Min { get; set; }
        public int Count { get; set; }

        public IEnumerable<Node> RootList
        {
            get
            {
                if (Min == null) yield break;
                yield return Min;
                foreach (var sibling in Min.Siblings)
                    yield return sibling;
            }
        }

        private string RootListString
        {
            get
            {
                if (Min == null) return string.Empty;

                var result = new StringBuilder();
                result.AppendFormat("{0}:{1}, ", Min.Key, Min.Value);
                foreach (var node in Min.Siblings)
                {
                    result.AppendFormat("{0}:{1}, ", node.Key, node.Value);
                }
                return result.Remove(result.Length-2, 2).ToString();
            }
        }
    }
}