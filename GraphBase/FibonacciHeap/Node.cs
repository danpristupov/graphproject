﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Dan.GraphBase.FibonacciHeap
{
    using TKey = System.Int32;
    using TValue = System.String;

    [DebuggerDisplay("{Key}: {Value}")]
    public class Item
    {
        public TKey Key { get; internal set; }
        public TValue Value { get; set; }
        internal Node Node { get; private set; }

        public Item(TKey key, TValue value)
        {
            Key = key;
            Value = value;
            Node = new Node(this);
        }
    }

    [DebuggerDisplay("{Key}: {Value}")]
    internal class Node
    {
        public Node(Item publicObject)
        {
            Degree = 0;
            Parent = null;
            Child = null;
            Mark = false;
            Left = Right = this;
            this.PublicObject = publicObject;
        }

        public TKey Key { get { return PublicObject.Key; }}
        public TValue Value { get { return PublicObject.Value; }}
        public Item PublicObject { get; set; }

        public Node Parent { get; set; }    // Reference to a parent
        
        public Node Left { get; set; }      // Left sibling
        public Node Right { get; set; }     // Right sibling

        public int Degree { get; set; }     // Number of children
        public Node Child { get; set; }     // Reference to any one of children

        public bool Mark { get; set; }      // Flag indicates whether the node has lost a child since the last
                                            //  time this node was made the child of another noted

        public IEnumerable<Node> Children
        {
            get
            {
                if (Child == null) yield break;
                // call Siblings
                var child = Child;
                do
                {
                    yield return child;
                    child = child.Right;
                } while (child != Child);
            }
        }
        public IEnumerable<Node> Siblings
        {
            get
            {
                var sibling = Right;
                while (sibling != this)
                {
                    yield return sibling;
                    sibling = sibling.Right;
                }
            }
        }

        public void AddSibling(Node newNode)
        {
            if (this == Right) // single node list
            {
                Right = Left = newNode;
                newNode.Left = newNode.Right = this;
            }
            else
            {
                var right = Right;

                Right = right.Left = newNode;
                newNode.Left = this;
                newNode.Right = right;
            }
        }

        public void AddChild(Node node)
        {
            node.Parent = this;
            if (Child == null)
                Child = node;
            else
                Child.AddSibling(node);
            
            Degree++;
        }

        public void RemoveChild(Node node)
        {
            if (node == node.Right)
                Child = null;
            else
            {
                if (node == Child)
                    Child = node.Right;

                node.Right.Left = node.Left;
                node.Left.Right = node.Right;
            }
            node.Parent = null;
            node.Left = node.Right = node;
            Degree--;
        }
        public void RemoveItselfFromTheList()
        {
            if (this == Right)
                throw new InvalidOperationException("Node is the only node in the list and cannot be removed");

            this.Right.Left = this.Left;
            this.Left.Right = this.Right;
            this.Left = this.Right = this; // make one-item circular list
        }
    }
}