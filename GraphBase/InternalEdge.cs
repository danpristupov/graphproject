﻿using System.Diagnostics;

namespace Dan.GraphBase
{
    [DebuggerDisplay("Dst: {Dst.PublicObject.Key} Weight: {Weight}")]
    internal class InternalEdge
    {
        public InternalVertex Src { get; private set; }
        public InternalVertex Dst { get; private set; }
        public int Weight;
        public object Tag { get; private set; }
        public Edge PublicObject { get; set; }

        public InternalEdge(InternalVertex source, InternalVertex destination, int weight)
        {
            Src = source;
            Dst = destination;
            Weight = weight;
        }
    }
}