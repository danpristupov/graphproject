﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Dan.GraphBase
{
    [DebuggerDisplay("Public key: {PublicObject.Key}")]
    internal class InternalVertex
    {
        public List<InternalEdge> Edges { get; private set; }
        public object Tag { get; set; }
        public Vertex PublicObject { get; set; }

        public InternalVertex(Vertex publicObject)
        {
            this.Edges = new List<InternalEdge>();
            this.PublicObject = publicObject;
        }

        public InternalEdge GetEdge(InternalVertex destination)
        {
            foreach (var edge in Edges)
                if (edge.Dst == destination) return edge;
            throw new KeyNotFoundException();
        }
    }
}