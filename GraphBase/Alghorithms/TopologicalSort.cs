﻿using System.Collections.Generic;
using System.Linq;

namespace Dan.GraphBase.Alghorithms
{
    [Algorithm(Category = "Elementary Graph Algorithms")]
    public class TopologicalSort : Algorithm
    {
        public TopologicalSort(Graph graphObject)
            :base(graphObject)
        {
        }

        public void Run(string source)
        {
            Run();
        }

        public void Run()
        {
            this.State.ResetState();
            var inDegree = new Dictionary<string, int>();
            foreach (var vertex in this.Graph.Vertices)
                inDegree[vertex.PublicObject.Key] = 0;

            var queue = new Queue<InternalVertex>();
            var result = new LinkedList<Vertex>();

            foreach (var vertex in this.Graph.Vertices)
            {
                if (inDegree[vertex.PublicObject.Key] == 0)
                    queue.Enqueue(vertex);
            }

            while (queue.Count != 0)
            {
                var vertex = queue.Dequeue();
                result.AddFirst(vertex.PublicObject); // yield return vertex.PublicObject
                foreach (var edge in vertex.Edges)
                {
                    if (--inDegree[edge.Dst.PublicObject.Key] == 0)
                        queue.Enqueue(edge.Dst);
                }
            }
            // todo: how to check if there are cycles??
            // 1. if queue is empty and there are vertices in inDegree with non zero degree
            // 2. we can add counter of the non-null vertices in the inDegree dictionary and decrement it on every enqueue
        }

        public override void RunExample(params Vertex[] vertices)
        {
            Run();
        }
    }
}