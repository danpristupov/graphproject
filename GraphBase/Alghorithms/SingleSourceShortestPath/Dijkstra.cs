﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dan.GraphBase.Alghorithms.SingleSourceShortestPath
{
    /// <summary>
    /// Dijkstra's algorithm solves the single-source shortest-paths tree problem
    /// Introduction to Algorithms 3rd ed, p658
    /// </summary>
    [Algorithm(Category = "Single Source Shortest Path")]
    public class Dijkstra : Algorithm
    {
        public Dijkstra(Graph graphObject)
            : base(graphObject)
        {
        }

        public override void RunExample(params Vertex[] vertices)
        {
            InternalVertex src;
            InternalVertex dst;
            if (vertices.Length >= 2)
            {
                src = Graph.FindVertexByPublicKey(vertices[0].Key);
                dst = Graph.FindVertexByPublicKey(vertices[1].Key);
            }
            else
            {
                var rand = GetRandomVertices(2);
                src = rand[0];
                dst = rand[1];
            }
            Run(src, dst);
        }

        public Vertex[] Run(string source, string destination)
        {
            var src = this.Graph.FindVertexByPublicKey(source);
            var dst = this.Graph.FindVertexByPublicKey(destination);

            return Run(src, dst);
        }

        private Vertex[] Run(InternalVertex src, InternalVertex dst)
        {
            var q = new PriorityQueue<InternalVertex>();

            InitializeSingleSource(q, src);
            this.State.SetStartVertex(src);
            this.State.SetEndVertex(dst);

            while (!q.IsEmpty())
            {
                Wait();
                var u = q.Dequeue();
                if (GetTag(u).Visited) continue;
                GetTag(u).Visited = true;

                this.State.SetActiveVertex(u);
                foreach (var edge in u.Edges)
                {
                    if (GetTag(edge.Dst).Visited) continue;
                    Wait();
                    this.State.SetActiveEdge(edge);
                    Relax(edge);
                    q.Enqueue(edge.Dst, (int) GetTag(edge.Dst).ShortestPath);
                }
            }
            this.State.SetActiveVertex(null);
            this.State.ClearEdges();
            ClearAdditionalInformation();
            return CreateAndHighlightPath(dst).ToArray();
        }

        private IEnumerable<Vertex> CreateAndHighlightPath(InternalVertex dst)
        {
            var vertex = dst;
            while (vertex != null)
            {
                var parent = GetTag(vertex).Parent;
                if (parent != null)
                    this.State.SetResultEdge(parent.GetEdge(vertex));

                yield return vertex.PublicObject;
                vertex = parent;
            }
        }

        private void Relax(InternalEdge edge)
        {
            var srcTag = GetTag(edge.Src);
            var dstTag = GetTag(edge.Dst);
            var newPath = srcTag.ShortestPath + edge.Weight;
            if (dstTag.ShortestPath > newPath)
            {
                dstTag.ShortestPath = newPath;
                edge.Dst.PublicObject.AdditionalInfo = GetVertexAdditionalInformation(edge.Dst);
                if (dstTag.Parent != null)
                    this.State.ResetState(dstTag.Parent.GetEdge(edge.Dst));
                dstTag.Parent = edge.Src;

                this.State.SetResultEdge(edge);
            }
        }

        private void InitializeSingleSource(PriorityQueue<InternalVertex> priorityQueue, InternalVertex src)
        {
            foreach (var vertex in this.Graph.Vertices)
            {
                vertex.Tag = new Tag {ShortestPath = double.PositiveInfinity, Parent = null};
                vertex.PublicObject.AdditionalInfo = GetVertexAdditionalInformation(vertex);
            }

            GetTag(src).ShortestPath = 0;
            priorityQueue.Enqueue(src, (int)GetTag(src).ShortestPath);
            this.State.ResetState();
        }

        private string GetVertexAdditionalInformation(InternalVertex value)
        {
            const char INFINITY_SYMBOL = (char) 0x221e;

            var result = new StringBuilder("Path: ");

            if (GetTag(value).Visited) result.Append("*");
            if (double.IsPositiveInfinity(GetTag(value).ShortestPath))
                result.Append(INFINITY_SYMBOL);
            else
                result.Append(GetTag(value).ShortestPath);
            return result.ToString();
        }

        private Tag GetTag(InternalVertex vertex)
        {
            return (Tag) vertex.Tag;
        }
        private class Tag
        {
            public double ShortestPath;
            public InternalVertex Parent;
            public bool Visited;
        }
    }
}