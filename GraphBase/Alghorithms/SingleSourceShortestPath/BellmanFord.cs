﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dan.GraphBase.Alghorithms.SingleSourceShortestPath
{
    using TVertexKey = String;

    /// <summary>
    /// The Bellman-Ford algorithm solves the single-source shortest-paths tree problem
    /// Introduction to Algorithms 3rd ed, p651
    /// </summary>
    [Algorithm(Category = "Single Source Shortest Path")]
    public class BellmanFord : Algorithm
    {
        public BellmanFord(Graph graphObject)
            : base(graphObject)
        {
        }

        public override void RunExample(params Vertex[] vertices)
        {
            InternalVertex src;
            InternalVertex dst;
            if (vertices.Length >= 2)
            {
                src = Graph.FindVertexByPublicKey(vertices[0].Key);
                dst = Graph.FindVertexByPublicKey(vertices[1].Key);
            }
            else
            {
                var rand = GetRandomVertices(2);
                src = rand[0];
                dst = rand[1];
            }
            Run(src, dst);
        }

        public Vertex[] Run(TVertexKey source, TVertexKey destination)
        {
            var src = this.Graph.FindVertexByPublicKey(source);
            var dst = this.Graph.FindVertexByPublicKey(destination);

            return Run(src, dst);
        }

        private Vertex[] Run(InternalVertex src, InternalVertex dst)
        {
            InitializeSingleSource(src);
            this.State.SetStartVertex(src);
            this.State.SetEndVertex(dst);

            for (var i = 0; i < Graph.Vertices.Count - 1; i++)
            {
                this.State.ClearEdges();
                foreach (var edge in Graph.Edges)
                {
                    this.Wait();
                    this.State.SetActiveEdge(edge);
                    Relax(edge);
                }
                HighlightPath(dst);
                this.Wait();
            }
            // check for a negative loops
            foreach (var edge in Graph.Edges)
            {
                if (GetTag(edge.Dst).ShortestPathEstimate > GetTag(edge.Src).ShortestPathEstimate + edge.Weight)
                    throw new InvalidOperationException("Negative circle has found");
            }
            this.State.ClearEdges();
            ClearAdditionalInformation();
            return CreateAndHighlightPath(dst).ToArray();
        }

        private void HighlightPath(InternalVertex dst)
        {
            var vertex = dst;
            while (vertex != null)
            {
                var parent = GetTag(vertex).Parent;
                if (parent != null)
                    this.State.SetResultEdge(parent.GetEdge(vertex));
                vertex = parent;
            }
        }
        private IEnumerable<Vertex> CreateAndHighlightPath(InternalVertex dst)
        {
            var vertex = dst;
            while (vertex != null)
            {
                var parent = GetTag(vertex).Parent;
                if (parent != null)
                    this.State.SetResultEdge(parent.GetEdge(vertex));

                yield return vertex.PublicObject;
                vertex = parent;
            }
        }
        private void Relax(InternalEdge edge)
        {
            var srcTag = GetTag(edge.Src);
            var dstTag = GetTag(edge.Dst);
            var newPath = srcTag.ShortestPathEstimate + edge.Weight;
            if (dstTag.ShortestPathEstimate > newPath)
            {
                dstTag.ShortestPathEstimate = newPath;
                edge.Dst.PublicObject.AdditionalInfo = GetVertexAdditionalInformation(edge.Dst);

                dstTag.Parent = edge.Src;
            }
        }

        private void InitializeSingleSource(InternalVertex src)
        {
            foreach (var vertex in this.Graph.Vertices)
            {
                vertex.Tag = new Tag {ShortestPathEstimate = double.PositiveInfinity, Parent = null};
                vertex.PublicObject.AdditionalInfo = GetVertexAdditionalInformation(vertex);
            }

            GetTag(src).ShortestPathEstimate = 0;
            this.State.ResetState();
        }

        private string GetVertexAdditionalInformation(InternalVertex value)
        {
            const char INFINITY_SYMBOL = (char)0x221e;
            var result = new StringBuilder("Path: ");

            if (double.IsPositiveInfinity(GetTag(value).ShortestPathEstimate))
                result.Append(INFINITY_SYMBOL);
            else
                result.Append(GetTag(value).ShortestPathEstimate);
            return result.ToString();
        }

        private Tag GetTag(InternalVertex vertex)
        {
            return (Tag) vertex.Tag;
        }
        private class Tag
        {
            public double ShortestPathEstimate;
            public InternalVertex Parent;
        }
    }
}