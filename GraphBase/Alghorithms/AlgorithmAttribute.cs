﻿using System;

namespace Dan.GraphBase.Alghorithms
{
    public class AlgorithmAttribute: Attribute
    {
        public string Category { get; set; }
    }
}