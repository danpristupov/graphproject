﻿using System.Collections;
using System.Collections.Generic;

namespace Dan.GraphBase.Alghorithms
{
    internal class AlgorithmStateNullObject : AlgorithmState
    {
        private readonly CollectionStub<Edge> _highlightEdges;
        private readonly CollectionStub<Vertex> _highlightedVertices;
        internal AlgorithmStateNullObject()
        {
            _highlightEdges = new CollectionStub<Edge>();
            _highlightedVertices = new CollectionStub<Vertex>();
        }

        private class CollectionStub<T> : ICollection<T>, IReadOnlyCollection<T>
        {
            public IEnumerator<T> GetEnumerator() { yield break; }
            IEnumerator IEnumerable.GetEnumerator() { yield break; }
            public void Add(T item) { }
            public void Clear() { }
            public bool Contains(T item) { return false; }
            public void CopyTo(T[] array, int arrayIndex) { }
            public bool Remove(T item) { return false; }
            public int Count { get { return 0; } }
            public bool IsReadOnly { get { return false; } }
        }

        internal override void SetStartVertex(InternalVertex vertex) { }
        internal override void SetEndVertex(InternalVertex vertex) { }
        internal override void SetResultVertex(InternalVertex vertex) { }
        internal override void SetActiveVertex(InternalVertex vertex) { }
        internal override void ResetState(InternalVertex vertex) { }
        internal override void SetResultEdge(InternalEdge edge) { }
        internal override void ResetState() { }
        internal override void SetActiveEdge(InternalEdge edge) { }
        internal override void ResetState(InternalEdge edge) { }
        internal override void ClearEdges() { }
    }
}