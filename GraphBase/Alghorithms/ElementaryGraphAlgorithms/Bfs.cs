﻿using System.Collections.Generic;
using System.Linq;

namespace Dan.GraphBase.Alghorithms.ElementaryGraphAlgorithms
{
    [Algorithm(Category = "Elementary Graph Algorithms")]
    public class Bfs : Algorithm
    {
        public Bfs(Graph graphObject)
            :base(graphObject)
        {
        }

        private enum Color
        {
            White, Gray, Black
        }
        public void Run(string source)
        {
            Run(Graph.FindVertexByPublicKey(source));
        }

        private void Run(InternalVertex src)
        {
            this.State.ResetState();

            var queue = new Queue<InternalVertex>();
            queue.Enqueue(src);

            var colors = this.Graph.Vertices.ToDictionary(vertex => vertex, vertex => Color.White);
            colors[src] = Color.Gray;

            var distances = this.Graph.Vertices.ToDictionary(vertex => vertex, vertex => int.MaxValue);
            distances[src] = 0;

            this.State.SetStartVertex(src);

            while (queue.Count != 0)
            {
                var vertex = queue.Dequeue();
                this.State.SetActiveVertex(vertex);
                Wait();
                foreach (var edge in vertex.Edges)
                {
                    if (colors[edge.Dst] == Color.White)
                    {
                        distances[edge.Dst] = distances[vertex] + 1;
                        colors[edge.Dst] = Color.Gray;
                        queue.Enqueue(edge.Dst);
                        this.State.SetResultEdge(edge);
                    }
                }
                colors[vertex] = Color.Black;
            }
        }

        public override void RunExample(params Vertex[] vertices)
        {
            InternalVertex src;
            if (vertices.Length > 0)
                src = Graph.FindVertexByPublicKey(vertices[0].Key);
            else
                src = GetRandomVertices(1)[0];
            Run(src);
        }
    }
}