﻿using System.Collections.Generic;

namespace Dan.GraphBase.Alghorithms.ElementaryGraphAlgorithms
{
    [Algorithm(Category = "Elementary Graph Algorithms")]
    public class Dfs : Algorithm
    {
        public Dfs(Graph graph)
            :base(graph)
        {
        }

        public override void RunExample(params Vertex[] vertices)
        {
            InternalVertex src;
            if (vertices.Length > 0)
                src = Graph.FindVertexByPublicKey(vertices[0].Key);
            else
                src = GetRandomVertices(1)[0];
            Run(src);
        }
        
        public void Run(string source)
        {
            Run(Graph.FindVertexByPublicKey(source));
        }

        private void Run(InternalVertex src)
        {
            this.State.ResetState();
            this.State.SetStartVertex(src);
            Run(src, new List<InternalVertex>());
        }

        private void Run(InternalVertex src, IList<InternalVertex> visited)
        {
            if (visited.Contains(src)) return;
            this.State.SetActiveVertex(src);
            Wait();
            visited.Add(src);

            foreach (var child in src.Edges)
            {
                this.State.SetResultEdge(child);
                Run(child.Dst, visited);
            }
        }
    }
}