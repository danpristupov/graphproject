﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dan.GraphBase.Alghorithms.MinimumSpanningTrees
{
    using TVertexKey = String;
    /// <summary>
    /// Kruskal's algorithm for solving the minimum spanning tree problem
    /// Introduction to Algorithms 3rd ed, p631
    /// </summary>
    [Algorithm(Category = "Minimum Spanning Trees")]
    public class Kruskal : Algorithm
    {
        private struct WeightTag
        {
            public int Weight;
        }

        public Kruskal(Graph graphObject)
            :base(graphObject)
        {
        }

        public override void RunExample(params Vertex[] vertices)
        {
            Run();
        }

        public void Run()
        {
            this.State.ResetState();

            var resultForest = new LinkedList<InternalEdge>();

            Dictionary<InternalVertex, LinkedList<InternalVertex>> trees = this.Graph.Vertices
                .Select(vertex => new
                {
                    Vertex = vertex,
                    Set = MakeSet(vertex)
                })
                .ToDictionary(_ => _.Vertex, _ => _.Set);

            var sortedEdges = this.Graph.Vertices
                .SelectMany(vertex => vertex.Edges,
                    (vertex, edge) => new
                    {
                        Src = vertex,
                        Edge = edge
                    })
                .OrderBy(_=>_.Edge.Weight);

            foreach (var edge in sortedEdges)
            {
                this.State.SetActiveEdge(edge.Edge);
                Wait();

                var srcSet = FindSet(trees, edge.Src);
                var dstSet = FindSet(trees, edge.Edge.Dst);
                if (srcSet != dstSet) // edge is safe!
                {
                    UnionSets(trees, srcSet, dstSet);
                    this.State.SetResultEdge(edge.Edge);
                    resultForest.AddLast(edge.Edge);
                }
            }
            this.State.SetActiveEdge(null);
        }

        private LinkedList<InternalVertex> MakeSet(InternalVertex vertex)
        {
            return new LinkedList<InternalVertex>(new[] { vertex });
        }

        private static void UnionSets(Dictionary<InternalVertex, LinkedList<InternalVertex>> trees, InternalVertex src, InternalVertex dst)
        {
            // union the two linked lists
            foreach (var vertex in trees[dst])
            {
                trees[src].AddLast(vertex);
            }
            trees.Remove(dst);
        }

        // Returns a head of set, which contains the vertex
        private InternalVertex FindSet(Dictionary<InternalVertex, LinkedList<InternalVertex>> trees, InternalVertex vertexToFind)
        {
            foreach (var tree in trees)
            {
                foreach (var vertex in tree.Value)
                {
                    if (vertex == vertexToFind) return tree.Value.First.Value;
                }
            }
            throw new InvalidOperationException();
        }
    }
}