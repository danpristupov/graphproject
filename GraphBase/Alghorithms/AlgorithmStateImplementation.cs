﻿namespace Dan.GraphBase.Alghorithms
{
    public class AlgorithmStateImplementation : AlgorithmState
    {
        private readonly GraphContainer _graph;

        internal AlgorithmStateImplementation(GraphContainer container)
        {
            _graph = container;
        }

        private Vertex _startVertex;
        internal override void SetStartVertex(InternalVertex vertex)
        {
            vertex.PublicObject.AlgorythmState = AlgorythmState.Start;
            _startVertex = vertex.PublicObject;
        }

        private Vertex _endVertex;
        internal override void SetEndVertex(InternalVertex vertex)
        {
            vertex.PublicObject.AlgorythmState = AlgorythmState.End;
            _endVertex = vertex.PublicObject;
        }

        private Vertex _resultVertex;
        internal override void SetResultVertex(InternalVertex vertex)
        {
            vertex.PublicObject.AlgorythmState = AlgorythmState.Result;
            _resultVertex = vertex.PublicObject;
        }

        private Vertex _activeVertex;
        internal override void SetActiveVertex(InternalVertex vertex)
        {
            if (_activeVertex != null)
            {
                if (_activeVertex == _resultVertex)
                    _activeVertex.AlgorythmState = AlgorythmState.Result;
                else if (_activeVertex == _startVertex)
                    _activeVertex.AlgorythmState = AlgorythmState.Start;
                else if (_activeVertex == _endVertex)
                    _activeVertex.AlgorythmState = AlgorythmState.End;
                else 
                _activeVertex.AlgorythmState = AlgorythmState.None;
            }
            if (vertex == null) return;
            _activeVertex = vertex.PublicObject;
            _activeVertex.AlgorythmState = AlgorythmState.Active;
        }

        internal override void ResetState(InternalVertex vertex)
        {
            vertex.PublicObject.AlgorythmState = AlgorythmState.None;
        }

        private Edge _activeEdge;
        internal override void SetActiveEdge(InternalEdge edge)
        {
            if (_activeEdge != null && _activeEdge.AlgorythmState == AlgorythmState.Active)
                _activeEdge.AlgorythmState = AlgorythmState.None;
            if (edge != null)
            {
                _activeEdge = edge.PublicObject;
                _activeEdge.AlgorythmState = AlgorythmState.Active;
            }
        }

        internal override void ResetState(InternalEdge edge)
        {
            edge.PublicObject.AlgorythmState = AlgorythmState.None;
        }

        internal override void ClearEdges()
        {
            foreach (var vertex in _graph.Vertices)
                foreach (var edge in vertex.Edges)
                    edge.PublicObject.AlgorythmState = AlgorythmState.None; ;
        }


        internal override void SetResultEdge(InternalEdge edge)
        {
            edge.PublicObject.AlgorythmState = AlgorythmState.Result;
        }

        internal override void ResetState()
        {
            _activeEdge = null;
            _activeVertex = _endVertex = _startVertex = _resultVertex = null;
            foreach (var vertex in _graph.Vertices)
            {
                vertex.PublicObject.AlgorythmState = AlgorythmState.None;
                foreach (var edge in vertex.Edges)
                    edge.PublicObject.AlgorythmState = AlgorythmState.None;;
            }
        }
    }
}