﻿using System;
using System.Linq;

namespace Dan.GraphBase.Alghorithms
{
    public abstract class Algorithm
    {
        protected Graph GraphObject { get; set; }

        internal Algorithm(Graph publicGraph)
        {
            GraphObject = publicGraph; // rename to something. GraphObject's not beautiful
            this.Graph = publicGraph.Container;
            if (publicGraph is EducationalGraph)
            {
                this.StepByStep = true;
                this.State = new AlgorithmStateImplementation(publicGraph.Container);
            }
            else
                this.State = new AlgorithmStateNullObject();

        }

        public abstract void RunExample(params Vertex[] vertices);

        internal GraphContainer Graph { get; private set; }
        internal AlgorithmState State { get; private set; }

        public bool StepByStep { get; set; }

        private bool _wait = true;
        public void Step()
        {
            _wait = false;
        }

        protected void Wait()
        {
            if (!StepByStep) return;
            _wait = true;
            while (_wait)
            {
                System.Threading.Thread.Sleep(100);
            }
        }

        internal InternalVertex[] GetRandomVertices(int number)
        {
            if (this.Graph.Vertices.Count < number) throw new InvalidOperationException(string.Format("Graph doesn't contain {0} vertices.", number));
            var result = new InternalVertex[number];
            var rnd = new Random((int)DateTime.Now.Ticks%10000);
            for (var i = 0; i < number; i++)
            {
                while (true)
                {
                    var randomIndex = rnd.Next(0, this.Graph.Vertices.Count);
                    if (result.Contains(this.Graph.Vertices[randomIndex]))
                        continue;
                    result[i] = this.Graph.Vertices[randomIndex];
                    break;
                }
            }
            return result;
        }

        protected void ClearAdditionalInformation()
        {
            foreach (var vertex in this.Graph.Vertices)
                vertex.PublicObject.AdditionalInfo = string.Empty;
        }
    }
}