﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Dan.GraphBase.Annotations;

namespace Dan.GraphBase.Alghorithms
{
    public abstract class AlgorithmState
    {
        internal abstract void ResetState();

        internal abstract void SetStartVertex(InternalVertex vertex);
        internal abstract void SetEndVertex(InternalVertex vertex);
        internal abstract void SetResultVertex(InternalVertex vertex);
        internal abstract void SetActiveVertex(InternalVertex vertex);
        internal abstract void ResetState(InternalVertex vertex);

        internal abstract void SetResultEdge(InternalEdge edge);
        internal abstract void SetActiveEdge(InternalEdge edge);
        internal abstract void ResetState(InternalEdge edge);

        internal abstract void ClearEdges();

        // Implementation of the INotifyPropertyChanged interface
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}