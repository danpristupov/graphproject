﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dan.GraphBase
{
    using TVertexKey = String;
    internal class GraphContainer
    {
        public List<InternalVertex> Vertices { get; private set; }

        public GraphContainer()
        {
            this.Vertices = new List<InternalVertex>();
        }

        public IEnumerable<InternalEdge> Edges
        {
            get { return Vertices.SelectMany(vertex => vertex.Edges); }
        }

        public InternalVertex FindVertexByPublicKey(TVertexKey key)
        {
            return this.Vertices.First(_ => _.PublicObject.Key == key);
        }
    }
}