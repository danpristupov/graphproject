﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Dan.GraphBase;
using Dan.GraphBase.Alghorithms;
using Dan.GraphWpf.GraphView;

namespace Dan.GraphWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string _readmeMessage = @"
Hi!
Few notes about this app:

    * Select single or multiple vertices using Ctrl+Click.
    * Drag and drop selected vertices as you wish.
    * For creating a new vertex double click on empty space or press Ctrl+V.
    * For creating a new edge select two vertices and press Ctrl+E.
    * Double click on vertex/edge to edit.
    * Two selected vertices (source and destination) required for both Dijkstra and Bellman-Ford algs
    * Select single vertex required for DFS/BFS algs.
    * If there're no selected vertices, random ones will be used.

Regards,
Dan
danil@danil.cz
";
        private readonly Graph _graph = new EducationalGraph();
        public MainWindow()
        {
            InitializeComponent();
            txtReadme.Text = _readmeMessage;
            _graphControl.DataContext = _graph;
            _graphControl.VertexAdding += GraphControlOnVertexAdding;
            _graphControl.VertexEditing += GraphControlOnVertexEditing;
            _graphControl.EdgeEditing += GraphControlOnEdgeEditing;
            _graphControl.PropertyChanged += GraphControlOnPropertyChanged;
            _graphControl.AlgorithmStarted += GraphControlOnAlgorithmStarted;
            _graphControl.AlgorithmFinished += GraphControlOnAlgorithmFinished;
        }


        private void GraphControlOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Busy")
                BtnStart.IsEnabled = BtnGenerate.IsEnabled = cbxAlgorithms.IsEnabled = !_graphControl.Busy;
        }

        private void GraphControlOnVertexAdding(object sender, VertexAddingEventArgs vertexAddingEventArgs)
        {
            var vertexEditDialog = EditVertexDialog.ShowCreateVertexDialog();
            var result = vertexEditDialog.ShowDialog();
            if (result == false)
            {
                vertexAddingEventArgs.Cancel = true;
                return;
            }

            vertexAddingEventArgs.Key = vertexEditDialog.Key;
        }

        private void GraphControlOnVertexEditing(object sender, VertexEditingEventArgs vertexEditingEventArgs)
        {
            var vertexEditDialog = EditVertexDialog.ShowEditVertexDialog(vertexEditingEventArgs.Vertex);
            vertexEditDialog.ShowDialog();
        }

        private void GraphControlOnEdgeEditing(object sender, EdgeEditingEventArgs edgeEditingEventArgs)
        {
            var edgeEditDialog = EditEdgeDialog.ShowEditEdgeDialog(edgeEditingEventArgs.Edge); 
            edgeEditDialog.ShowDialog();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var items = Assembly.GetAssembly(typeof (Algorithm)).GetTypes()
                .Where(_ => _.IsSubclassOf(typeof (Algorithm)))
                .Select(_ => new AlgorithmComboboxItem
                {
                    Name = _.Name,
                    Type = _,
                    Category = _.GetAttributeValue((AlgorithmAttribute alg) => alg.Category)
                })
                .ToList();
            var lcv = new ListCollectionView(items);
            lcv.GroupDescriptions.Add(new PropertyGroupDescription("Category"));
            cbxAlgorithms.ItemsSource = lcv;
            GraphGenerator.GenerateGraph(_graph);
        }

        private void GenerateGraph(object sender, RoutedEventArgs e)
        {
            GraphGenerator.GenerateGraph(_graph);
        }

        private Algorithm _activeAlgorithm;
        private void GraphControlOnAlgorithmFinished(object sender, Algorithm algorithm)
        {
            _activeAlgorithm = null;
            BtnNextStep.Visibility = Visibility.Hidden;
        }

        private void GraphControlOnAlgorithmStarted(object sender, Algorithm algorithm)
        {
            _activeAlgorithm = algorithm;
            BtnNextStep.Visibility = Visibility.Visible;
        }
        private void NextStep(object sender, RoutedEventArgs e)
        {
            _activeAlgorithm.Step();
        }

        private void StartAlgorythm(object sender, RoutedEventArgs e)
        {
            if (cbxAlgorithms.SelectedItem as AlgorithmComboboxItem == null) return;
            var algorithm = (Algorithm) Activator.CreateInstance(
                (cbxAlgorithms.SelectedItem as AlgorithmComboboxItem).Type,
                _graph);
            _graphControl.RunAlgorithm(algorithm);
        }

        private void WindowOnKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) && e.Key == Key.V)
            {
                var vertexEditDialog = EditVertexDialog.ShowCreateVertexDialog();
                if (vertexEditDialog.ShowDialog() != true) return;

                _graph.AddVertex(vertexEditDialog.Key);
            }
            else if (Keyboard.IsKeyDown(Key.LeftCtrl) && e.Key == Key.E)
            {
                if (_graphControl.SelectedVertices.Count == 2)
                {
                    var createEdgeDialog = EditEdgeDialog.ShowCreateEdgeDialog(_graphControl.SelectedVertices);
                    if (createEdgeDialog.ShowDialog() != true) return;
                    _graph.AddEdge(_graphControl.SelectedVertices[0].Vertex.Key,
                        _graphControl.SelectedVertices[1].Vertex.Key,
                        createEdgeDialog.Weight
                        );
                }
            }
        }

        private class AlgorithmComboboxItem
        {
            public Type Type { get; set; }
            public string Name { get; set; }
            public string Category { get; set; }
        }
    }
}
