﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Dan.GraphBase;
using Dan.GraphBase.Alghorithms;
using Dan.GraphBase.Annotations;

namespace Dan.GraphWpf.GraphView
{
    public partial class GraphView : Canvas, INotifyPropertyChanged
    {
        public delegate void VertexAddingEventHandler(object sender, VertexAddingEventArgs e);
        public delegate void VertexEditingEventHandler(object sender, VertexEditingEventArgs e);
        public delegate void EdgeEditingEventHandler(object sender, EdgeEditingEventArgs e);

        private readonly BackgroundWorker _backgroundWorker = new BackgroundWorker();

        public GraphView()
        {
            InitializeComponent();

            this.SizeChanged += Graph_SizeChanged;
            this.LayoutTransform = new ScaleTransform(1, 1);
            this.DataContextChanged += GraphVisualiser_DataContextChanged;

            this.Background = Brushes.Transparent;
            this.PreviewMouseLeftButtonDown += OnPreviewMouseLeftButtonDown;
        }


        void OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _startPointOnCanvas = e.GetPosition((UIElement)sender);

            var hitTestResult = VisualTreeHelper.HitTest(this, _startPointOnCanvas);
            if (hitTestResult == null) return;

            if (ClickedOnEmptySpace(hitTestResult))
            {
                ClearSelection();
                if (IsDoubleClick(e) && !_busy) AddVertex();
                return;
            }

            var target = GetAncestorOrDefaultOf<GraphViewElement>(hitTestResult.VisualHit);
            if (IsDoubleClick(e))
            {
                if (!_busy)
                    EditSelectedElement(target);
                return;
            }

            if (target is VertexElement)
            {
                var targetVertex = target as VertexElement;
                if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    AddSelection(targetVertex);
                else
                {
                    if (!_selectedVertices.Contains(target))
                    {
                        SetSelection(targetVertex);
                    }
                }
                UpdateCursorDiffsForSelectedElements(e);
            }
        }

        private void UpdateCursorDiffsForSelectedElements(MouseButtonEventArgs e)
        {
            _selectedElementsCursorDiffs.Clear();
            foreach (var selectedVertex in _selectedVertices)
                _selectedElementsCursorDiffs.Add(selectedVertex, e.GetPosition(selectedVertex));
        }

        private static bool ClickedOnEmptySpace(HitTestResult hitTestResult)
        {
            return hitTestResult.VisualHit is GraphView;
        }

        private void EditSelectedElement(GraphViewElement targetElelement)
        {
            if (targetElelement is VertexElement)
                EditVertex(targetElelement as VertexElement);
            if (targetElelement is EdgeElement)
                EditEdge(targetElelement as EdgeElement);
        }

        #region Vertex Selection

        private readonly List<VertexElement> _selectedVertices = new List<VertexElement>();
        public IList<VertexElement> SelectedVertices
        {
            get { return _selectedVertices.AsReadOnly(); }
        }

        private void AddSelection(VertexElement vertex)
        {
            if (_selectedVertices.Contains(vertex))
            {
                _selectedVertices.Remove(vertex);
                vertex.IsSelected = false;
            }
            else
            {
                _selectedVertices.Add(vertex);
                vertex.IsSelected = true;
            }
            OnPropertyChanged("SelectedVertices");
        }

        private void SetSelection(VertexElement vertex)
        {
            foreach (var v in _selectedVertices)
                v.IsSelected = false;
            _selectedVertices.Clear();
            _selectedVertices.Add(vertex);
            vertex.IsSelected = true;
            OnPropertyChanged("SelectedVertices");
        }
        private void ClearSelection()
        {
            if (_selectedVertices.Count == 0) return;
            foreach (var v in _selectedVertices)
                v.IsSelected = false;

            _selectedVertices.Clear();
            OnPropertyChanged("SelectedVertices");
        }
        #endregion

        #region DragNdrop
        private Point _startPointOnCanvas;
        private readonly Dictionary<VertexElement, Point> _selectedElementsCursorDiffs
            = new Dictionary<VertexElement, Point>();

        protected override void OnMouseMove(MouseEventArgs mouseEventArgs)
        {
            base.OnMouseMove(mouseEventArgs);
            if (mouseEventArgs.LeftButton != MouseButtonState.Pressed) return;
            if (_selectedVertices.Count == 0) return;

            var mousePos = mouseEventArgs.GetPosition(this);
            if (MouseReachedMinimumDragDistance(mousePos, _startPointOnCanvas)) return;

            foreach (var selectedVertex in _selectedVertices)
            {
                var cursorPointOnTarget = _selectedElementsCursorDiffs[selectedVertex];
                selectedVertex.SetValue(Canvas.LeftProperty, mousePos.X - cursorPointOnTarget.X);
                selectedVertex.SetValue(Canvas.TopProperty, mousePos.Y - cursorPointOnTarget.Y);
            }
        }

        private bool MouseReachedMinimumDragDistance(Point currentPoint, Point startPoint)
        {
            var diff = startPoint - currentPoint;
            return Math.Abs(diff.X) < SystemParameters.MinimumHorizontalDragDistance
                   && Math.Abs(diff.Y) < SystemParameters.MinimumVerticalDragDistance;
        }

        #endregion


        public event VertexAddingEventHandler VertexAdding;
        public event VertexEditingEventHandler VertexEditing;
        public event EdgeEditingEventHandler EdgeEditing;
        public event EventHandler<Algorithm> AlgorithmStarted;
        public event EventHandler<Algorithm> AlgorithmFinished;

        private void AddVertex()
        {
            var key = string.Empty;
            var callback = VertexAdding;
            if (callback != null)
            {
                var args = new VertexAddingEventArgs();
                callback(this, args);
                if (args.Cancel) return;
                key = args.Key;
            }
            var lastSixSymbols = new Func<string, string>(n => n.Substring(n.Length - 6, 6));
            key = string.IsNullOrEmpty(key)
                ? lastSixSymbols(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture))
                : key;

            Graph.AddVertex(key);
        }

        private void EditVertex(VertexElement vertexElement)
        {
            var callback = VertexEditing;
            if (callback == null) return;
            callback(this, new VertexEditingEventArgs {Vertex = vertexElement.Vertex});
        }

        private void EditEdge(EdgeElement edgeElement)
        {
            var callback = EdgeEditing;
            if (callback == null) return;
            callback(this, new EdgeEditingEventArgs { Edge = edgeElement.Edge });
        }

        private static bool IsDoubleClick(MouseButtonEventArgs e)
        {
            return e.ClickCount == 2;
        }
        private static T GetAncestorOrDefaultOf<T>(DependencyObject target) where T: UIElement
        {
            while (!(target is T) && (target != null))
            {
                target = VisualTreeHelper.GetParent(target);
            }
            return target as T;
        }

        void GraphVisualiser_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldGraph = (e.OldValue as Graph);
            if (oldGraph != null)
            {
                oldGraph.GraphChanged -= Graph_GraphChanged;
            }

            var newGraph = (e.NewValue as Graph);
            if (newGraph != null)
            {
                newGraph.GraphChanged += Graph_GraphChanged;
                RefreshGraphElements();
            }
        }

        private void RefreshGraphElements()
        {
            if (Graph == null) return;
            var i = 0;
            var vertexLocations = Graph.Vertices
                .ToDictionary(vertex => vertex.Key, vertex => CalculateVertexLocations(i++));
            this.Children.Clear();
            foreach (var vertex in Graph.Vertices)
            {
                var location = vertexLocations[vertex.Key];
                if (vertex.Tag == null)
                    vertex.Tag = new VertexElement(vertex);

                var vrtElem = (VertexElement)vertex.Tag;
                vrtElem.SetValue(Canvas.LeftProperty, location.X);
                vrtElem.SetValue(Canvas.TopProperty, location.Y);
                this.Children.Add(vrtElem);
                foreach (var edge in vertex.Edges)
                {
                    this.Children.Add(new EdgeElement((VertexElement) edge.Source.Tag,
                        (VertexElement) edge.Destination.Tag, edge));
                }
            }

        }

        void Graph_GraphChanged(object sender, EventArgs e)
        {
            RefreshGraphElements();
        }

        private Graph Graph
        {
            get { return this.DataContext as Graph; }
        }

        void Graph_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            RefreshGraphElements();
        }

        private Point CalculateVertexLocations(int vertex)
        {
            const double shift  = 0.31337;
            var x0 = this.RenderSize.Width / 2.0;
            var y0 = this.RenderSize.Height / 2.0;
            var radius = (Math.Min(this.RenderSize.Height, this.RenderSize.Width) / 2) * 0.85;
            return new Point(
                x0 + radius * Math.Cos(shift + 2 * Math.PI * vertex / Graph.VertexCount),
                y0 + radius * Math.Sin(shift + 2 * Math.PI * vertex / Graph.VertexCount)
                );
        }

        private bool _busy;
        public bool Busy
        {
            get { return _busy; }
            internal set
            {
                if (_busy == value) return;
                _busy = value;
                OnPropertyChanged();
            }
        }

        public void RunAlgorithm(Algorithm algorithm)
        {
            algorithm.StepByStep = true;
            var doWork = new DoWorkEventHandler((e, s) =>
            {
                algorithm.RunExample(_selectedVertices.Select(_ => _.Vertex).ToArray());
            });
            _backgroundWorker.DoWork += doWork;

            _backgroundWorker.RunWorkerCompleted += (sender, args) =>
            {
                OnAlgorithmFinished(algorithm);
                _backgroundWorker.DoWork -= doWork;
                Busy = false;
            };
            Busy = true;
            _backgroundWorker.RunWorkerAsync();
            OnAlgorithmStarted(algorithm);
        }

        private void OnAlgorithmStarted(Algorithm algorithm)
        {
            var callback = AlgorithmStarted;
            if (callback == null) return;
            callback(this, algorithm);
        }

        private void OnAlgorithmFinished(Algorithm algorithm)
        {
            var callback = AlgorithmFinished;
            if (callback == null) return;
            callback(this, algorithm);
        }

        // Implementation of the INotifyPropertyChanged interface
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}