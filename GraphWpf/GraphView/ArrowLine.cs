﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Dan.GraphWpf.GraphView
{
    public class ArrowLine : Shape
    {
        protected PathGeometry Pg;
        protected PathFigure Pf;
        protected PolyLineSegment Pls;
        private readonly PathFigure _pfStartArrow;
        private readonly PathFigure _pfEndArrow;

        public ArrowLine()
        {
            Pg = new PathGeometry();
            Pf = new PathFigure();
            Pls = new PolyLineSegment();
            Pf.Segments.Add(Pls);
            _pfStartArrow = new PathFigure();
            _pfStartArrow.Segments.Add(new PolyLineSegment());
            _pfEndArrow = new PathFigure();
            _pfEndArrow.Segments.Add(new PolyLineSegment());
        }

        #region public Dependency Properties

        public static readonly DependencyProperty X1Property = RegisterDependencyProperty("X1", 0.0);
        public double X1
        {
            set { SetValue(X1Property, value); }
            get { return (double) GetValue(X1Property); }
        }

        public static readonly DependencyProperty Y1Property = RegisterDependencyProperty("Y1", 0.0);
        public double Y1
        {
            set { SetValue(Y1Property, value); }
            get { return (double) GetValue(Y1Property); }
        }

        public static readonly DependencyProperty X2Property = RegisterDependencyProperty("X2", 0.0);
        public double X2
        {
            set { SetValue(X2Property, value); }
            get { return (double)GetValue(X2Property); }
        }

        public static readonly DependencyProperty Y2Property = RegisterDependencyProperty("Y2", 0.0);
        public double Y2
        {
            set { SetValue(Y2Property, value); }
            get { return (double)GetValue(Y2Property); }
        }
        
        // Specify the arrowhead size in the x direction:
        public static readonly DependencyProperty ArrowheadSizeXProperty =
            RegisterDependencyProperty("ArrowheadSizeX", 5.0);
        public double ArrowheadSizeX
        {
            set { SetValue(ArrowheadSizeXProperty, value); }
            get { return (double)GetValue(ArrowheadSizeXProperty); }
        }

        // Specify the arrowhead size in the y direction:
        public static readonly DependencyProperty ArrowheadSizeYProperty =
            RegisterDependencyProperty("ArrowheadSizeY", 10.0);
        public double ArrowheadSizeY
        {
            set { SetValue(ArrowheadSizeYProperty, value); }
            get { return (double)GetValue(ArrowheadSizeYProperty); }
        }

        // Specify arrowhead ends:
        public static readonly DependencyProperty ArrowheadEndProperty =
            RegisterDependencyProperty("ArrowheadEnd", ArrowheadEndEnum.End);
        public ArrowheadEndEnum ArrowheadEnd
        {
            set { SetValue(ArrowheadEndProperty, value); }
            get { return (ArrowheadEndEnum)GetValue(ArrowheadEndProperty); }
        }
        
        public static readonly DependencyProperty IsArrowheadClosedProperty =
            RegisterDependencyProperty("IsArrowheadClosed", false);
        public bool IsArrowheadClosed
        {
            set { SetValue(IsArrowheadClosedProperty, value); }
            get { return (bool)GetValue(IsArrowheadClosedProperty); }
        }

        #endregion
        
        private static DependencyProperty RegisterDependencyProperty<T>(string name, T defaultValue)
        {
            return DependencyProperty.Register(name, typeof(T), typeof(ArrowLine),
                new FrameworkPropertyMetadata(defaultValue, FrameworkPropertyMetadataOptions.AffectsMeasure));
        }

        protected override Geometry DefiningGeometry
        {
            get
            {
                Pg.Figures.Clear();
                Pf.StartPoint = new Point(X1, Y1);
                Pls.Points.Clear();
                Pls.Points.Add(new Point(X2, Y2));
                Pg.Figures.Add(Pf);
                if (Pls.Points.Count <= 0) return Pg;

                var pt1 = new Point();
                var pt2 = new Point();
                if ((ArrowheadEnd & ArrowheadEndEnum.Start) == ArrowheadEndEnum.Start)
                {
                    pt1 = Pf.StartPoint;
                    pt2 = Pls.Points[0];
                    Pg.Figures.Add(CreateArrowhead(_pfStartArrow, pt2, pt1));
                }
                if ((ArrowheadEnd & ArrowheadEndEnum.End) == ArrowheadEndEnum.End)
                {
                    pt1 = Pls.Points.Count == 1 ? Pf.StartPoint : Pls.Points[Pls.Points.Count - 2];
                    pt2 = Pls.Points[Pls.Points.Count - 1];
                    Pg.Figures.Add(CreateArrowhead(_pfEndArrow, pt1, pt2));
                }
                return Pg;
            }
        }

        private PathFigure CreateArrowhead(PathFigure pathFigure, Point pt1, Point pt2)
        {
            var pt = new Point();
            var v = new Vector();
            Matrix matrix = ArrowheadTransform(pt1, pt2);
            var pls1 = pathFigure.Segments[0] as PolyLineSegment;
            pls1.Points.Clear();
            if (!IsArrowheadClosed)
            {
                v = new Point(0, 0) - new Point(ArrowheadSizeX/2, ArrowheadSizeY);
                pt = pt2 + v*matrix;
                pathFigure.StartPoint = pt;
                pls1.Points.Add(pt2);
                v = new Point(0, 0) - new Point(-ArrowheadSizeX/2, ArrowheadSizeY);
                pt = pt2 + v*matrix;
                pls1.Points.Add(pt);
            }
            else if (IsArrowheadClosed)
            {
                v = new Point(0, 0) - new Point(ArrowheadSizeX/2, 0);
                pt = pt2 + v*matrix;
                pathFigure.StartPoint = pt;
                v = new Point(0, 0) - new Point(0, -ArrowheadSizeY);
                pt = pt2 + v*matrix;
                pls1.Points.Add(pt);
                v = new Point(0, 0) - new Point(-ArrowheadSizeX/2, 0);
                pt = pt2 + v*matrix;
                pls1.Points.Add(pt);
            }
            pathFigure.IsClosed = IsArrowheadClosed;
            return pathFigure;
        }

        private static Matrix ArrowheadTransform(Point pt1, Point pt2)
        {
            var matrix = new Matrix();
            double theta = 180*(Math.Atan((pt2.X - pt1.X)/(pt2.Y - pt1.Y)))/Math.PI;
            double dx = pt2.X - pt1.X;
            double dy = pt2.Y - pt1.Y;
            if (dx >= 0 && dy >= 0)
                theta = -theta;
            else if (dx < 0 && dy >= 0)
                theta = -theta;
            else if (dx < 0 && dy < 0)
                theta = 180 - theta;
            else if (dx >= 0 && dy < 0)
                theta = 180 - theta;
            matrix.Rotate(theta);
            return matrix;
        }
    }

    public enum ArrowheadEndEnum
    {
        None = 0,
        Start = 1,
        End = 2,
        Both = 3
    }
}
