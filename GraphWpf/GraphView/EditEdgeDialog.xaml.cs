﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using Dan.GraphBase;

namespace Dan.GraphWpf.GraphView
{
    public partial class EditEdgeDialog : Window
    {
        private readonly IList<VertexElement> _selectedVertices;
        private readonly Edge _edge;

        public static EditEdgeDialog ShowCreateEdgeDialog(IList<VertexElement> selectedVertices)
        {
            return new EditEdgeDialog(selectedVertices);
        }
        public static EditEdgeDialog ShowEditEdgeDialog(Edge edge)
        {
            return new EditEdgeDialog(edge);
        }

        public EditEdgeDialog()
        {
            InitializeComponent();
            this.Loaded += OnLoaded;
        }
        private EditEdgeDialog(Edge edge)
            : this()
        {
            _edge = edge;
        }
        private EditEdgeDialog(IList<VertexElement> selectedVertices)
            : this()
        {
            _selectedVertices = selectedVertices;
        }

        public int Weight { get; set; }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (_edge == null)
            {
                this.Title = "Create Edge";
                btnOk.Content = "Create";
                txtEdge.Text = string.Format("{0} -> {1}", _selectedVertices[0].Vertex.Key, _selectedVertices[1].Vertex.Key);
                txtWeight.Text = GenerateWeight().ToString();
            }
            else
            {
                this.Title = "Edit edge";
                btnOk.Content = "Save";
                txtEdge.Text = string.Format("{0} -> {1}", _edge.Source.Key, _edge.Destination.Key);
                txtWeight.Text = _edge.Weight.ToString();
            }
            txtWeight.SelectAll();
            txtWeight.Focus();
        }

        private int GenerateWeight()
        {
            return (int)(DateTime.Now.Ticks%100);
        }


        private void OnCancel(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void OnOk(object sender, RoutedEventArgs e)
        {
            var weight = Convert.ToInt32(txtWeight.Text);
            if (_edge != null)
                _edge.Weight = weight;
            else
                Weight = weight;
            this.DialogResult = true;
            this.Close();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                OnCancel(this, new RoutedEventArgs());
            else if (e.Key == Key.Return)
                OnOk(this, new RoutedEventArgs());
        }

        private void TxtWeightOnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = new Regex("[^0-9.-]+").IsMatch(e.Text);
        }
    }
}
