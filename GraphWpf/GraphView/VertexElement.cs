﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Shapes;
using Dan.GraphBase;

namespace Dan.GraphWpf.GraphView
{
    // todo: Add button for sorting (as circle, as sphere etc)
    public class VertexElement : GraphViewElement, INotifyPropertyChanged
    {
        private readonly Vertex _vertex;
        private bool _isSelected;
        public const int CircleRadius = 10; // todo: create property in style in resources

        public VertexElement(Vertex vertex)
        {
            _vertex = vertex;
            _vertex.PropertyChanged += VertexOnPropertyChanged;
            this.DataContext = vertex;

            var canvas = new Canvas();
            canvas.Children.Add(CreateEllipse());
            canvas.Children.Add(CreateText(_vertex));
            canvas.Children.Add(CreateAdditionalInfoText(_vertex));
            this.Content = canvas;
        }

        void VertexOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "AlgorythmState")
                OnPropertyChanged("AlgorythmState");
        }

        public AlgorythmState AlgorythmState
        {
            get { return _vertex.AlgorythmState; }
        }

        public Vertex Vertex
        {
            get { return _vertex; }
        }

        private static TextBlock CreateAdditionalInfoText(Vertex vertex)
        {
            var additionalInfo = new TextBlock
            {
                Tag = "AdditionalInfoTextBlock"
            };
            additionalInfo.DataContext = vertex;
            additionalInfo.SetBinding(TextBlock.TextProperty, new Binding("AdditionalInfo"));
            return additionalInfo;
        }

        private static TextBlock CreateText(Vertex vertex)
        {
            var text = new TextBlock
            {
                Tag = "KeyTextBlock",
            };
            text.DataContext = vertex;
            text.SetBinding(TextBlock.TextProperty, new Binding("Key"));
            return text;
        }

        private static Ellipse CreateEllipse()
        {
            var circle = new Ellipse
            {

            };
            return circle;
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            internal set
            {
                if (_isSelected == value) return;
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}