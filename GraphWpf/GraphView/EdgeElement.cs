﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Dan.GraphBase;

namespace Dan.GraphWpf.GraphView
{
    public class EdgeElement : GraphViewElement, INotifyPropertyChanged
    {
        private readonly VertexElement _source;
        private readonly VertexElement _destination;
        private readonly Edge _edge;
        private readonly ArrowLine _arrowLine;
        private readonly TextBlock _weightTextBlock;

        public EdgeElement(VertexElement source, VertexElement destination, Edge edge)
        {
            _source = source;
            _destination = destination;
            _edge = edge;
            _edge.PropertyChanged += EdgeOnPropertyChanged;
            _arrowLine = CreateArrowLine();
            _weightTextBlock = CreateWeightTextBlock(edge);
            var canvas = new Canvas();
            canvas.Children.Add(_arrowLine);
            canvas.Children.Add(_weightTextBlock);
            this.Content = canvas;
            UpdateEdgePosition();
            SubscribeToVerticesEvents(source, destination);
        }

        private void EdgeOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "AlgorythmState")
                OnPropertyChanged("AlgorythmState");
        }

        public AlgorythmState AlgorythmState
        {
            get { return _edge.AlgorythmState; }
        }

        public Edge Edge
        {
            get { return _edge; }
        }
        private TextBlock CreateWeightTextBlock(Edge edge)
        {
            var textBlock = new TextBlock();
            textBlock.DataContext = edge;
            textBlock.SetBinding(TextBlock.TextProperty, new Binding("Weight"));
            return textBlock;
        }

        private static ArrowLine CreateArrowLine()
        {
            return new ArrowLine();
        }

        private void SubscribeToVerticesEvents(VertexElement source, VertexElement destination)
        {
            var leftPropertydescriptor =
                DependencyPropertyDescriptor.FromProperty(Canvas.LeftProperty, typeof (VertexElement));
            var topPropertydescriptor =
                DependencyPropertyDescriptor.FromProperty(Canvas.TopProperty, typeof (VertexElement));

            leftPropertydescriptor.AddValueChanged(source, UpdateEdgePosition);
            topPropertydescriptor.AddValueChanged(source, UpdateEdgePosition);
            leftPropertydescriptor.AddValueChanged(destination, UpdateEdgePosition);
            topPropertydescriptor.AddValueChanged(destination, UpdateEdgePosition);
        }

        private void UpdateEdgePosition(object sender, EventArgs eventArgs)
        {
            UpdateEdgePosition();
        }
        private void UpdateEdgePosition()
        {
            var srcPoint = GetCenterOfCircle(_source);
            var dstPoint = GetCenterOfCircle(_destination);
            var startPoint = IntersectionOfCircleAndLine(srcPoint, srcPoint, dstPoint);
            var endPoint = IntersectionOfCircleAndLine(dstPoint, srcPoint, dstPoint);

            _arrowLine.X1 = startPoint.X;
            _arrowLine.Y1 = startPoint.Y;
            _arrowLine.X2 = endPoint.X;
            _arrowLine.Y2 = endPoint.Y;

            UpdateWeightTextBlockPosition(MidPointOfTheLine(startPoint, endPoint));
        }

        private void UpdateWeightTextBlockPosition(Point point)
        {
            _weightTextBlock.SetValue(Canvas.LeftProperty, point.X);
            _weightTextBlock.SetValue(Canvas.TopProperty, point.Y);
        }

        private static Point GetCenterOfCircle(VertexElement element)
        {
            return new Point(
                (double)element.GetValue(Canvas.LeftProperty) + VertexElement.CircleRadius,
                (double)element.GetValue(Canvas.TopProperty) + VertexElement.CircleRadius);
        }
        private static Point MidPointOfTheLine(Point start, Point end)
        {
            return new Point((start.X + end.X) / 2, (start.Y + end.Y) / 2);
        }

        private static Point IntersectionOfCircleAndLine(Point circleCenter, Point lineStart, Point lineEnd)
        {
            const int CIRCLE_RADIUS = 8;
            // circle: (x-a)^2+(y-b)^2=r^2 line: y=mx+c
            // line: Ax+By+C=0

            // solve problem with m=0 (vertical line cannot be described as y=mx+c)
            if (Math.Abs(lineEnd.X - lineStart.X) < 0.001) lineStart = new Point(lineStart.X + 0.1, lineStart.Y);

            var m = (lineEnd.Y - lineStart.Y) / (lineEnd.X - lineStart.X);
            var c = -m * lineStart.X + lineStart.Y;

            var aprim = 1 + Math.Pow(m, 2.0);
            var bprim = 2 * m * (c - circleCenter.Y) - 2 * circleCenter.X;
            var cprim = Math.Pow(circleCenter.X, 2) + Math.Pow(c - circleCenter.Y, 2) - Math.Pow(CIRCLE_RADIUS + 3, 2);
            var delta = Math.Pow(bprim, 2) - 4 * aprim * cprim;

            // intersection1
            var x1 = (-bprim + Math.Sqrt(delta)) / (2 * aprim);
            var y1 = m * x1 + c;
            var point1 = new Point(x1, y1);

            // intersection2
            var x2 = (-bprim - Math.Sqrt(delta)) / (2 * aprim);
            var y2 = m * x2 + c;
            var point2 = new Point(x2, y2);

            var farPoint = DistanceBetweenPoints(circleCenter, lineStart) > DistanceBetweenPoints(circleCenter, lineEnd) ? lineStart : lineEnd;

            // return intersection point close to the opposite end of the line
            return DistanceBetweenPoints(point1, farPoint) < DistanceBetweenPoints(point2, farPoint) ? point1 : point2;
        }
        private static double DistanceBetweenPoints(Point one, Point other)
        {
            return Math.Sqrt(Math.Pow(one.X - other.X, 2) + Math.Pow(one.Y - other.Y, 2));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}