﻿using System;
using System.Collections.Generic;
using Dan.GraphBase;

namespace Dan.GraphWpf.GraphView
{
    // todo: should not be part of the graph!
    public class GraphGenerator
    {
        private static readonly List<string> _names;

        static GraphGenerator()
        {
            _names = new List<string>
            {
                "Ahmedabad",
                "Atlanta",
                "Bangkok",
                "Bengaluru",
                "Barcelona",
                "BeloHorizonte",
                "Beijing",
                "Berlin",
                "Bogotá",
                "BuenosAires",
                "Cairo",
                "Chengdu",
                "Chennai",
                "Chicago",
                "Chongqing",
                "Dallas–FortWorth",
                "Delhi",
                "Dhaka",
                "Guangzhou",
                "HoChiMinhCity",
                "Houston",
                "HongKong-Shenzhen",
                "Hyderabad",
                "Istanbul",
                "Jakarta",
                "Karachi",
                "Kinshasa",
                "Kolkata",
                "Lagos",
                "Lima",
                "London",
                "LosAngeles",
                "Madrid",
                "Manila",
                "MexicoCity",
                "Miami",
                "Moscow",
                "Mumbai",
                "Nagoya",
                "NewYork",
                "Osaka-Kobe-Kyoto(Keihanshin)",
                "Paris",
                "Philadelphia",
                "Pune",
                "Ruhr",
                "RiodeJaneiro",
                "SãoPaulo",
                "Santiago",
                "Shanghai",
                "Seoul",
                "Singapore",
                "Taipei",
                "Tehran",
                "Tokyo",
                "Toronto",
                "Washington,D.C.",
            };
        }
        public static void GenerateGraph(Graph graph)
        {
            graph.Clear();
            var rnd = new Random();
            var vertexCnt = rnd.Next() % 10 + 7;
            var edgeCnt = (rnd.Next() % vertexCnt + 5) * 2;

            var vertices = new List<int>(edgeCnt);
            var edges = new Dictionary<KeyValuePair<int, int>, bool>(edgeCnt);
            for (var i = 0; i < vertexCnt; i++)
                vertices.Add(i);
            for (var i = 0; i < edgeCnt; i++)
            {
                while (true)
                {
                    var src = rnd.Next() % vertexCnt;
                    var dst = rnd.Next() % vertexCnt;
                    var edge = new KeyValuePair<int, int>(src, dst);

                    if (edges.ContainsKey(edge)) continue;
                    edges.Add(edge, false);
                    break;
                }
            }

            foreach (var vertex in vertices)
                graph.AddVertex(_names[vertex]);
            foreach (var edge in edges)
            {
                var pair = edge.Key;
                graph.AddEdge(_names[pair.Key], _names[pair.Value], pair.Key + pair.Value);
            }

        }
    }
}
