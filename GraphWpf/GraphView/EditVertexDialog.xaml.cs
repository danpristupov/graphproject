﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Input;
using Dan.GraphBase;

namespace Dan.GraphWpf.GraphView
{
    public partial class EditVertexDialog : Window
    {
        private readonly Vertex _vertex;

        public static EditVertexDialog ShowEditVertexDialog(Vertex vertex)
        {
            return new EditVertexDialog(vertex);
        }

        public static EditVertexDialog ShowCreateVertexDialog()
        {
            return new EditVertexDialog(null);
        }

        private EditVertexDialog(Vertex vertex)
        {
            InitializeComponent();

            _vertex = vertex;
            this.Loaded += OnLoaded;
        }

        public string Key { get; set; }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (_vertex == null)
            {
                this.Title = "Create vertex";
                btnOk.Content = "Create";
                txtKey.Text = GenerateKey();
            }
            else
            {
                this.Title = "Edit vertex";
                btnOk.Content = "Save";
                txtKey.Text = _vertex.Key;
            }
            txtKey.Focus();
            txtKey.SelectAll();
        }

        private string GenerateKey()
        {
            var lastSixSymbols = new Func<string, string>(n => n.Substring(n.Length - 6, 6));
            return lastSixSymbols(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture));
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            if (_vertex != null)
                _vertex.Key = txtKey.Text;
            else
                Key = txtKey.Text;

            this.DialogResult = true;
            this.Close();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Escape)
                OnCancel(this, new RoutedEventArgs());
            else if (e.Key == System.Windows.Input.Key.Return)
                OnOK(this, new RoutedEventArgs());
        }
    }
}
