﻿using System;
using Dan.GraphBase;

namespace Dan.GraphWpf.GraphView
{
    public class VertexAddingEventArgs : EventArgs
    {
        public string Key { get; set; }
        public bool Cancel { get; set; }
    }
    public class VertexEditingEventArgs : EventArgs
    {
        public Vertex Vertex { get; set; }
    }
    public class EdgeEditingEventArgs : EventArgs
    {
        public Edge Edge { get; set; }
    }
}