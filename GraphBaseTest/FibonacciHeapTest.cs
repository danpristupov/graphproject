﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dan.GraphBase.FibonacciHeap;
using NUnit.Framework;

namespace GraphBaseTest
{
    [TestFixture]
    public class FibonacciHeapTest
    {
        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void HeapThrowsInvalidOperationExceptionWhenExtractingValueFromEmptyHeap()
        {
            var fib = new FibonacciHeap();
            fib.ExtractMin();
        }

        [Test]
        public void FibonacciHeapBasicTest_1()
        {
            var fib = new FibonacciHeap();
            fib.Insert(2, "1");
            fib.Insert(2, "2");
            fib.Insert(2, "3");
            fib.Insert(0, "7");
            fib.Insert(1, "4");
            fib.Insert(1, "5");
            fib.Insert(1, "6");

            Assert.AreEqual("7", fib.ExtractMin().Value);
            Assert.AreEqual("4", fib.ExtractMin().Value);
            Assert.AreEqual("5", fib.ExtractMin().Value);
            Assert.AreEqual("6", fib.ExtractMin().Value);
            Assert.AreEqual("1", fib.ExtractMin().Value);
            Assert.AreEqual("3", fib.ExtractMin().Value);
            Assert.AreEqual("2", fib.ExtractMin().Value);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void DecreaseKeyThrowsInvalidOperationExceptionWhenNewKeyIsGreaterThanTheCurrentOneTest()
        {
            var fib = new FibonacciHeap();
            var item = fib.Insert(2, "1");
            fib.DecreaseKey(item, 5);
        }

        [Test]
        public void DecreaseKeyTest_DegreeIs0()
        {
            var fib = new FibonacciHeap();
            var item = fib.Insert(3, "10");
            fib.Insert(2, "1");
            fib.Insert(2, "2");
            fib.Insert(2, "3");
            fib.Insert(1, "4");
            fib.Insert(1, "5");
            fib.Insert(1, "6");

            fib.DecreaseKey(item, 0);

            Assert.AreEqual("10", fib.ExtractMin().Value);
        }

        [Test]
        public void DecreaseKeyTest_DegreeMoreThan0()
        {
            var fib = new FibonacciHeap();
            var item = fib.Insert(30, "10");
            fib.Insert(20, "1");
            fib.Insert(20, "2");
            var three = fib.Insert(20, "3");
            fib.Insert(10, "4");
            fib.Insert(10, "5");
            fib.Insert(10, "6");
            fib.ExtractMin();
            fib.DecreaseKey(item, 9);
            fib.DecreaseKey(three, 9);
            fib.DecreaseKey(item, 7);
            Assert.AreEqual("10", fib.ExtractMin().Value);
            Assert.AreEqual("3", fib.ExtractMin().Value);
            Assert.AreEqual("5", fib.ExtractMin().Value);
        }

        [Test]
        public void DecreaseKeyTest_DegreeMoreThan1() // fragile test!
        {
            var count = 10000;
            var r = new Random();
            var fib = new FibonacciHeap();
            var items = new List<Item>(count);
            for (var i = 0; i < count; i++)
            {
                var val = r.Next();
                items.Add(fib.Insert(val%10000, val.ToString()));
            }
            var x = fib.ExtractMin();
            items.Remove(x);

            for (var i = 0; i < count-1; i++)
            {
                var item = items[r.Next()%(items.Count)];
                var newKey = item.Key/2;
                items.Remove(item);
                fib.DecreaseKey(item, newKey);
            }
            var result = new List<Item>(count);
            for (var i=0; i<count-1; i++)
                result.Add(fib.ExtractMin());
        }
    }
}
