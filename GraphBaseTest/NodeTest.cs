﻿using System;
using System.Linq;
using Dan.GraphBase.FibonacciHeap;
using NUnit.Framework;

namespace GraphBaseTest
{
    [TestFixture]
    public class NodeTest
    {
        [Test]
        public void NewNodeIsCircularList()
        {
            var target = CreateNode(1, "1");
            Assert.AreEqual(target, target.Left);
            Assert.AreEqual(target, target.Right);
        }

        [Test]
        public void RemoveChildWorksRemovesSingleChild()
        {
            var target = CreateNode(1, "1");
            var child = CreateNode(11, "11");
            target.AddChild(child);

            target.RemoveChild(child);



            Assert.IsNull(child.Parent);
            Assert.AreEqual(child, child.Left);
            Assert.AreEqual(child, child.Right);

            Assert.IsEmpty(target.Children);
            Assert.AreEqual(0, target.Degree);
            Assert.IsNull(target.Child);
        }

        [Test]
        public void RemoveChildWorksRemovesFirstOfMultipleChildren()
        {
            var target = CreateNode(1, "1");
            var firstChild = CreateNode(11, "11");
            var secondChild = CreateNode(12, "12");
            target.AddChild(firstChild);
            target.AddChild(secondChild);

            target.RemoveChild(firstChild);

            Assert.IsNull(firstChild.Parent);
            Assert.AreEqual(firstChild, firstChild.Left);
            Assert.AreEqual(firstChild, firstChild.Right);

            Assert.AreEqual(1, target.Degree);
            Assert.AreEqual(secondChild, target.Child);
            Assert.AreEqual(1, target.Children.Count());
        }
        
        [Test]
        public void RemoveChildWorksRemovesSecondOfMultipleChildren()
        {
            var target = CreateNode(1, "1");
            var firstChild = CreateNode(11, "11");
            var secondChild = CreateNode(12, "12");
            target.AddChild(firstChild);
            target.AddChild(secondChild);

            target.RemoveChild(secondChild);

            Assert.IsNull(secondChild.Parent);
            Assert.AreEqual(secondChild, secondChild.Left);
            Assert.AreEqual(secondChild, secondChild.Right);

            Assert.AreEqual(1, target.Degree);
            Assert.AreEqual(firstChild, target.Child);
            Assert.AreEqual(1, target.Children.Count());
        }

        private static Node CreateNode(int key, string value)
        {
            return new Item(key, value).Node;
        }

        [Test]
        public void FibonacciHeapBasicTest_1()
        {
        }
    }
}