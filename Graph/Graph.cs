﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dan.Graph
{
    public class Graph
    {
        private List<List<Edge>>  _edges = new List<List<Edge>>();

        public List<List<Edge>> Edges
        {
            get
            {
                return _edges;
            }
        }
    }

    public class Edge
    {
        public Vertex From { get; set; }
        public Vertex To { get; set; }
    }

    public class Vertex
    {
        public int Id { get; set; }
    }
}
